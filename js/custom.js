$(document).ready(function(){
	// get current time
	var dt = new Date();
	var ampm = (dt.getHours() >= 12) ? "PM" : "AM";
	var time = (dt.getHours()== 0?'12':dt.getHours()) + ":" + (dt.getMinutes()<10?'0':'') + dt.getMinutes()  +" "+ampm;
	//console.log( (dt.getMinutes()<10?'0':'') + dt.getMinutes() );
	console.log(dt.getHours());
	
	// search contacts function
	// filter contacts (not case-sensitive)
	var $rows = $('.nav-tabs li');
		$('.search-chat-users').keyup(function() {
			var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
			
			$rows.show().filter(function() {
				var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
				return !~text.indexOf(val);
		}).hide();
	});
	
	
	// function to add the new message to chat body
	function addNewMsg(){
		var newMsg = $('.new-sent-message').val();
		
		if(newMsg.length > 0){
			//console.log(time);
			
			//console.log($('.new-sent-message').val());
			
			$('.tab-content .tab-pane.active .chat-panel').append("<li class='chat-block chat-block-right pull-right'><p>"+newMsg+"</p><span>"+time+"</span></li>");
		
			bot();		
			console.log($(".new-sent-message").val());	
			// run bot function when enter key pressed
			questionNum++;	
			$('.new-sent-message').val('');
		}
		
	}
	
	// when clicking send button the text from input field will be added to the chat
	$('#send-msg').click(function(){
		addNewMsg();
		
	});
	
	// when pressing enter key the text from input field will be added to the chat
	$('.new-sent-message').keypress(function(e) {
		var key = e.which;
		if (key == 13) // the enter key code
		{
			addNewMsg();
			
			
		}
    });

	// moving new messages to the top and show notification with the new messages
	// this is repeated every 30 seconds 
	// and the notification disappears after 3 seconds.
	setInterval(function(){
		// generate random number between 1 and 6 to select a message to be the new one
		// and to move it to the top of the other messages.
		//console.log($('#chat-app-body .app-left-block .nav-tabs li').length);
		var contactNum = Math.floor(Math.random()*($('#chat-app-body .app-left-block .nav-tabs li').length-2)+1);
		console.log(contactNum);
		var contactElement = $('#chat-app-body .app-left-block .nav-tabs li:nth-child('+ contactNum +')');
		$(contactElement).addClass('new-msg');
		$(contactElement).find('.avatar-icon').removeClass('offline');
		$(contactElement).find('.time-meta').html(time);
		$(contactElement).css({'margin-left': '-200px'});
		$(contactElement).prependTo('#chat-app-body .app-left-block .nav-tabs').animate({"margin-left":"0px"}, "slow");
		$('#chat-app-body .app-left-block .nav-tabs :not(li:nth-child('+ contactNum +'))').slideDown();
		// message notification 
		// show new message notification with name and content
		$('#new-msg-notification .new-msg-notification-name').html($(contactElement).find('.app-left-block-name').html());
		//console.log($(contactElement).find('.app-left-block-name').html());
		$("#new-msg-notification").animate({'right':'25px'},1000).delay(2000).animate({'right':'-350px'},1000);
		
		// notification sound when receiving new message. 
		$('#buzzer').get(0).play();
	},30000);
	
	// remove new-msg style when opening the new message
	// check if the message is active and has .new-msg class
	// if true then remove this class.
	$('.nav-tabs li').click(function(){
		if('.active'){
			$(this).removeClass('new-msg');
			
			// if message is active we should change the name of the person we are chatting with
			$('.app-header-right-info span span').html($(this).find('.name-meta').html());
			$('.app-header-right-info img').attr('src', $(this).find('.avatar-icon img').attr('src'))
			//console.log($(this).find('.avatar-icon img').attr('src'));
			//console.log($('.app-header-right-info span span').html($(this).find('.name-meta').html()););
		}
	});
	
	// show and hide left & right block in mobile sizes
	var $window = $(window);
	var windowsize = $window.width();
		if (windowsize <= 500) {
		console.log('lll');
			$('.app-left-block .back-arrow, .nav-tabs li').click(function(){
			$('.app-left-block').fadeOut();
			$('.app-right-block').fadeIn();
		});
		$('.app-right-block .back-arrow').click(function(){
			$('.app-left-block').fadeIn();
			$('.app-right-block').fadeOut();
		});
	}

	
	// simulate simple chat bot
	
	// keep count of question, used for IF condition.
	var questionNum = 0;
														
	// first question
	var question = '<p>Hello Dear :) What is your name?</p>';				  

	// ouput first question
	var output = $('#tab-6 .chat-panel');		
	$(output).append('<li class="chat-block chat-block-left pull-left"><p>'+question+'</p><span>'+time+'</span></li>');		
	
	// chat bot function
	function bot() { 
		var input = $(".new-sent-message").val();
		//console.log(input);
		// check if it is the first question then output hello message
		// output response
		if (questionNum == 0) {
		$(output).append('<li class="chat-block chat-block-left pull-left"><p>Hello '+input+'</p><span>'+time+'</span></li>');		

		
		// load next question
		question = '<p>How old are you?</p>';			    	
		
		// output next question after 2sec delay
		setTimeout(timedQuestion, 2000);									
		
		}

		else if (questionNum == 1) {
		
		$(output).append('<li class="chat-block chat-block-left pull-left"><p>That means you were born in '+(2017 - input)+'</p><span>'+time+'</span></li>');
		//document.getElementById("input").value = "";   
		question = '<p>Where are you from?</p>';					      	
		setTimeout(timedQuestion, 2000);
		}   
		
	}
	
	// function to call for outputting questions
	function timedQuestion() {
		
		$(output).append('<li class="chat-block chat-block-left pull-left"><p>'+question+'</p><span>'+time+'</span></li>');
	}

});


